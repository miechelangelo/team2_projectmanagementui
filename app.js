// ------------------------------------ API ROUTES ----------------------------------------------
const stockServiceUrl = 'http://localhost:8080/api/v1/stock';

// ------------------------------------ FUNCTION DECLARATIONS ------------------------------------

function createTransactionsTable(stockJson) {
    // create table rows here!!

    let tableHtml = '<thead>\n' +
        '        <tr>\n' +
        '            <th scope="col">ID</th>\n' +
        '            <th scope="col">Stock Ticker</th>\n' +
        '            <th scope="col">Order Price ($)</th>\n' +
        '            <th scope="col">Order Size</th>\n' +
        '            <th scope="col">Order Type</th>\n' +
        '        </tr>\n' +
        '        </thead>'
    Object.values(stockJson).forEach( stock => {
        tableHtml += '<tr><td>' + stock.id + '</td><td>' + stock.ticker + '</td><td>' + parseFloat(stock.orderPrice).toFixed(2) + '</td><td>' + stock.orderSize + '</td><td>' + stock.orderType + '</td></tr>';
    });

    document.querySelector('#stockTable').innerHTML = tableHtml;
}

function updateStock() {
    fetchStock().then(createTransactionsTable);
}

async function fetchStock() {
    // http GET sent to Java program
    const response = await fetch(stockServiceUrl);
    // WHEN the Response comes back from Java program return json
    return response.json();
}

async function postStock(data){
    // http POST sent to Java program
    const response = await fetch(stockServiceUrl, {
        method: 'POST',
        mode: 'cors',
        cache: "default",
        credentials: "same-origin",
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: JSON.stringify(data)
    });

    return response.json();
}

function buyStock(){

    document.getElementById("buyButton").onclick = function (){
        let id = Math.floor(Math.random() * 100000000000) + 1;
        let stockTicker = document.getElementById("stockTicker").value;
        let price = document.getElementById("price").value;
        let numberOfShares = document.getElementById("numberOfShares").value;
        let data = {
            "id": id.toString(),
            "orderPrice": price,
            "orderSize": numberOfShares,
            "orderType": "Buy",
            "ticker": stockTicker
        }
        postStock(data).then(data => {
            console.log(data)
        })
    }
}

function sellStock(){

    document.getElementById("sellButton").onclick = function (){
        let id = Math.floor(Math.random() * 100000000000) + 1;
        let stockTicker = document.getElementById("stockTicker").value;
        let price = document.getElementById("price").value;
        let numberOfShares = document.getElementById("numberOfShares").value;
        let data = {
            "id": id.toString(),
            "orderPrice": price,
            "orderSize": numberOfShares,
            "orderType": "Sell",
            "ticker": stockTicker
        }
        postStock(data).then(data => {
            console.log(data)
        })
    }
}

function shortenStockObject(stock){

    let fin;

    stock.orderType === "Buy" ?
    fin = {
        "ticker" : stock.ticker,
        "orderPrice": stock.orderPrice,
        "currentPosition": parseFloat(stock.orderPrice) * parseFloat(stock.orderSize)
    }
        :
    fin = {
        "ticker" : stock.ticker,
        "orderPrice": stock.orderPrice,
        "currentPosition": -(parseFloat(stock.orderPrice) * parseFloat(stock.orderSize))
    }

    return fin;
}

async function getPositions(){
    let transactions = await fetchStock();
    let postions = {};
    for(let i = 0; i < transactions.length ; i++){
        let currentStock = transactions[i];
        let ticker = currentStock.ticker;
        if(ticker in postions){
            let temp = postions[ticker];
            postions[ticker] = {
                "ticker": ticker,
                "orderPrice": currentStock.orderPrice,
                "currentPosition": shortenStockObject(currentStock).currentPosition + temp.currentPosition
            }
        } else {
            postions[ticker] = shortenStockObject(currentStock);
        }
    }

    let balance = 0;
    for(let value of Object.values(postions)){
        balance = balance + value.currentPosition;
    }

    document.getElementById("currentBalance").innerHTML =
        balance > 0 ? "Current Balance: $" + balance.toFixed(2).toString() :
            "Current Balance: -$" + Math.abs(balance).toFixed(2).toString()

    createPositionsTable(postions);

    return postions;

}

function createPositionsTable(postions){

    console.log("Postions Table")
    console.log(postions)

    let tableHtml = '<thead>\n' +
        '        <tr>\n' +
        '            <th scope="col">Stock Ticker</th>\n' +
        '            <th scope="col">Order Price ($)</th>\n' +
        '            <th scope="col">Current Position ($)</th>\n' +
        '        </tr>\n' +
        '        </thead>'
    Object.values(postions).forEach( stock => {
        tableHtml += '<tr><td>' + stock.ticker + '</td><td>' + parseFloat(stock.orderPrice).toFixed(2) + '</td><td>' + parseFloat(stock.currentPosition).toFixed(2) + '</td></tr>';
    });

    document.querySelector('#positionsTable').innerHTML = tableHtml;

}


// ------------------------------------ FUNCTION USES ------------------------------------

updateStock();

buyStock();

sellStock();

getPositions().then(data => {
    console.log(data)
});
